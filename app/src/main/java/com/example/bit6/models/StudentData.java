package com.example.bit6.models;

import android.os.Parcel;
import android.os.Parcelable;

public class StudentData
        implements Parcelable {
    private Integer id;
    private String firstName;
    private String lastName;
    private String gender;
    private String country;
    private String phone;

    public StudentData(String firstName, String lastName, String gender, String country, String phone) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.country = country;
        this.phone = phone;
    }

    public StudentData(Integer id, String firstName, String lastName, String gender, String country, String phone) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.country = country;
        this.phone = phone;
    }

    protected StudentData(Parcel in) {
        if (in.readByte() == 0) { id = null; } else { id = in.readInt(); }
        firstName = in.readString();
        lastName = in.readString();
        gender = in.readString();
        country = in.readString();
        phone = in.readString();
    }

    public static final Creator<StudentData> CREATOR = new Creator<StudentData>() {
        @Override
        public StudentData createFromParcel(Parcel in) {
            return new StudentData(in);
        }

        @Override
        public StudentData[] newArray(int size) {
            return new StudentData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        if (id == null) { parcel.writeByte((byte) 0); } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(id);
        }
        parcel.writeString(firstName);
        parcel.writeString(lastName);
        parcel.writeString(gender);
        parcel.writeString(country);
        parcel.writeString(phone);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }


    public String getGender() {
        return gender;
    }


    public String getCountry() {
        return country;
    }


    public String getPhone() {
        return phone;
    }
}
