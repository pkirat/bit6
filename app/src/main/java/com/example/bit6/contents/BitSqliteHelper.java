package com.example.bit6.contents;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import com.example.bit6.utils.MyConstants;

import java.util.ArrayList;
import java.util.List;

public class BitSqliteHelper
        extends SQLiteOpenHelper {

    public BitSqliteHelper(@Nullable Context context) {
        super(context, "test.db", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("create table test_table (" +
                                       "id integer primary key not null, " +
                                       "name text," +
                                       "age integer," +
                                       "class text)"
                              );

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("drop table if exists test_table");
        onCreate(sqLiteDatabase);
    }

    public List<String> getAllRecords() {
        List<String> allDataList = new ArrayList<>();
        SQLiteDatabase database = this.getReadableDatabase();
        //Cursor cursor = database.rawQuery("select * from test_table", null);
        Cursor cursor = database.rawQuery("select " + MyConstants.STUDENT_FNAME + " from test_table", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            allDataList.add(cursor.getString(1));
            cursor.moveToNext();
        }
        cursor.close();
        return allDataList;
    }
}
