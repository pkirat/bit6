package com.example.bit6;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.bit6.models.StudentData;
import com.example.bit6.service.CustomService;
import com.google.android.material.button.MaterialButton;

import java.util.ArrayList;

public class MainActivity
        extends AppCompatActivity {

    //Declare variables with proper data types that needs to be manipulated
    private EditText inputFirstName;

    private EditText inputLastName;

    private RadioGroup radioGroupGender;

    private Spinner countrySpinner;

    private EditText inputPhoneNumber;

    private MaterialButton buttonSubmit;
    private MaterialButton buttonShowRecords;


    String firstName, lastName, gender = "", country, phone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //assign variables
        inputFirstName = findViewById(R.id.etFirstName);
        inputLastName = findViewById(R.id.etLastName);
        radioGroupGender = findViewById(R.id.rgGender);
        countrySpinner = findViewById(R.id.spnCountry);
        inputPhoneNumber = findViewById(R.id.etPhoneNumber);
        buttonSubmit = findViewById(R.id.btnSubmit);
        buttonShowRecords = findViewById(R.id.btnShowRecords);

        handleRadioGroup();
        handleCountryData();

        handleButtonClick();
    }

    //private void initializeCountrySpinner(){
    //    String countryArray[] = new String[]{"Nepal", "India", "Bhutan"};
    //    ArrayAdapter<String> adapter =
    //            new ArrayAdapter<>(this,
    //                               androidx.appcompat.R.layout.support_simple_spinner_dropdown_item,
    //                               countryArray);
    //    adapter.setDropDownViewResource(R.layout.spinner_text);
    //    countrySpinner.setAdapter(adapter);
    //}


    private void handleButtonClick() {
        //Submit btnShowRecords click
        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isFormValid()) {
                    passDataWithSharedPreferences();
                    //startService(new Intent(getApplicationContext(), CustomService.class));
                    startActivity(passDataWithIntent());
                }
            }
        });

        buttonShowRecords.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(passDataWithIntent());
            }
        });
    }

    private void handleRadioGroup() {
        // first get checked radio btnShowRecords from the RadioGroup
        radioGroupGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                RadioButton radioButton = findViewById(i);
                gender = radioButton.getText().toString();
            }
        });

    }

    private void handleCountryData() {
        //always check for the changes that might occur when a user selects different item
        countrySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                //assign value to 'country'
                country = adapterView.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    private Intent passDataWithIntent() {

        Intent intent = new Intent(MainActivity.this, SecondActivity.class);
        Bundle bundle = new Bundle();
        //method 1
        bundle.putString("fname", firstName);
        bundle.putString("lname", lastName);
        bundle.putString("gender", gender);
        bundle.putString("country", country);
        bundle.putString("phone", phone);


        //method 2
        intent.putExtra("fname", firstName);
        intent.putExtra("lname", lastName);
        intent.putExtra("gender", gender);
        intent.putExtra("country", country);
        intent.putExtra("phone", phone);

        //method 3
        ArrayList<StudentData> arrayList = new ArrayList<>();
        arrayList.add(new StudentData(firstName, lastName, gender, country, phone));
        bundle.putParcelableArrayList("studentList", arrayList);

        intent.putExtra("bundleData", bundle);
        return intent;
    }

    private void passDataWithSharedPreferences() {
        SharedPreferences sharedPreferences = getSharedPreferences("sharedPrefs", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("prefFirstName", firstName);
        editor.putString("prefLastName", lastName);
        editor.putString("prefGender", gender);
        editor.putString("prefCountry", country);
        editor.putString("prefPhone", phone);
        editor.apply();
    }

    private boolean validateFirstName() {
        if (!inputFirstName.getText().toString().isEmpty()) {
            firstName = inputFirstName.getText().toString();
            return true;
        }
        inputFirstName.setError("First name cannot be empty !");
        return false;
    }

    private boolean validateLastName() {
        if (!inputLastName.getText().toString().isEmpty()) {
            lastName = inputLastName.getText().toString();
            return true;
        }
        inputLastName.setError("Last name cannot be empty !");
        return false;
    }

    private boolean validatePhoneNumber() {
        if (inputPhoneNumber.getText().toString().length() == 10) {
            phone = inputPhoneNumber.getText().toString();
            return true;
        }
        inputPhoneNumber.setError("Phone number must contain 10 digits");
        return false;
    }

    private boolean validateGender() {
        if (!gender.isEmpty()) {
            return true;
        }
        Toast.makeText(this, "Select gender !", Toast.LENGTH_SHORT).show();
        return false;
    }

    private boolean isFormValid() {
        return validateFirstName() && validateLastName() && validateGender() && validatePhoneNumber();
    }

}