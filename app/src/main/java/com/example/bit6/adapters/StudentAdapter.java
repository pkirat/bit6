package com.example.bit6.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.bit6.R;
import com.example.bit6.models.StudentData;
import com.google.android.material.textview.MaterialTextView;

import java.util.ArrayList;

public class StudentAdapter
        extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;

    private ArrayList<StudentData> studentDataArrayList;

    public StudentAdapter(Context context) {
        this.mContext = context;
    }

    public StudentAdapter(Context context, ArrayList<StudentData> studentData){
        this.mContext = context;
        this.studentDataArrayList = studentData;
    }

    @NonNull
    @Override
    public StudentAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.student_records_list_item, parent, false);
        return new StudentAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        StudentAdapterViewHolder studentAdapterViewHolder = (StudentAdapterViewHolder) holder;
        StudentData data = studentDataArrayList.get(position);
        //studentAdapterViewHolder.tvId.setText(String.valueOf(data.getId()));
        studentAdapterViewHolder.tvFirstName.setText(data.getFirstName());
        studentAdapterViewHolder.tvLastName.setText(data.getLastName());
        studentAdapterViewHolder.tvGender.setText(data.getGender());
        studentAdapterViewHolder.tvCountry.setText(data.getCountry());
        studentAdapterViewHolder.tvPhone.setText(data.getPhone());
    }


    @Override
    public int getItemCount() {
        return studentDataArrayList == null ? 0 : studentDataArrayList.size();
    }


    private class StudentAdapterViewHolder
            extends RecyclerView.ViewHolder {

        MaterialTextView tvId;

        MaterialTextView tvFirstName;

        MaterialTextView tvLastName;

        MaterialTextView tvGender;

        MaterialTextView tvCountry;

        MaterialTextView tvPhone;

        public StudentAdapterViewHolder(View view) {
            super(view);
            //tvId = view.findViewById(R.id.tvId);
            tvFirstName = view.findViewById(R.id.tvFirstName);
            tvLastName = view.findViewById(R.id.tvLastName);
            tvGender = view.findViewById(R.id.tvGender);
            tvCountry = view.findViewById(R.id.tvCountry);
            tvPhone = view.findViewById(R.id.tvPhone);
        }
    }

    public void setData(ArrayList<StudentData> studentData){
        this.studentDataArrayList  = studentData;
        notifyDataSetChanged();
    }
}
